use chrono;
use clap::{crate_authors, crate_version, App, Arg};
use failure::Fallible;
use fern;
use log;
use num_traits::FromPrimitive;
use realsense_rust::{
    error::Error as RsError, error::Result as RsResult, kind::Rs2Option, options::ToOptions,
};
use realsense_sys;
use std::ffi::CStr;
use std::io;
use std::ptr;
use std::ptr::NonNull;

fn main() -> Fallible<()> {
    let app = App::new("rsdumpoptions")
        .about("Dump RealSense device options")
        .version(crate_version!())
        .author(crate_authors!())
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increases logging verbosity each use for up to 3 times"),
        );

    let app_args = app.get_matches();
    let verbosity: u64 = app_args.occurrences_of("verbose");
    setup_logging(verbosity)?;

    let ctx = realsense_rust::Context::new()?;
    let devices = ctx.query_devices(None)?;
    let dcount = devices.len().unwrap();

    if dcount == 0 {
        log::error!("No RS devices found");
        return Err(failure::err_msg("No RS devices found"));
    }

    for device in devices {
        let device = device.unwrap();
        let dtype = device.product_line().unwrap().unwrap();
        let sn = device.serial_number().unwrap().unwrap();
        println!("\n=====\nFound {} SN {}", dtype, sn);
        let sensors = device.query_sensors()?;
        for sensor_res in sensors {
            match sensor_res {
                Ok(sensor_any) => {
                    /*
                    This crashes on 0.5.1 because not all options codes are defined in librealsense
                    let options = sensor_any.to_options()?;
                     */

                    let options_ptr = sensor_any.get_options_ptr();
                    let sensor_ext = sensor_any.try_extend()?;
                    println!("extended {:?}", &sensor_ext);
                    unsafe {
                        let list_ptr = {
                            let mut checker = RsErrorChecker::new();
                            let list_ptr = realsense_sys::rs2_get_options_list(
                                options_ptr.as_ptr(),
                                checker.inner_mut_ptr(),
                            );
                            checker.check()?;
                            list_ptr
                        };

                        let len = {
                            let mut checker = RsErrorChecker::new();
                            let len = realsense_sys::rs2_get_options_list_size(
                                list_ptr,
                                checker.inner_mut_ptr(),
                            );
                            checker.check()?;
                            len
                        };

                        for index in 0..len {
                            let mut checker = RsErrorChecker::new();
                            let val = realsense_sys::rs2_get_option_from_list(
                                list_ptr,
                                index,
                                checker.inner_mut_ptr(),
                            );
                            checker.check()?;
                            let option = Rs2Option::from_u32(val);
                            match option {
                                Some(option) => {
                                    let handle = OptionHandle {
                                        ptr: options_ptr,
                                        option,
                                    };
                                    println!("  optionhdl {:?}", handle);
                                    let mut optmin: f32 = 0.0;
                                    let mut optmax: f32 = 0.0;
                                    let mut optstep: f32 = 0.0;
                                    let mut optdef: f32 = 0.0;
                                    let mut checker = RsErrorChecker::new();
                                    realsense_sys::rs2_get_option_range(
                                        handle.ptr.as_ptr(),
                                        option as u32,
                                        &mut optmin,
                                        &mut optmax,
                                        &mut optstep,
                                        &mut optdef,
                                        checker.inner_mut_ptr(),
                                    );
                                    match checker.check() {
                                        RsResult::Err(err) => {
                                            println!(
                                                "get_option_range failed: {}",
                                                err.error_message()
                                            );
                                        }
                                        RsResult::Ok(_) => {
                                            println!(
                                                "    min={}, max={}, step={}, def={}",
                                                optmin, optmax, optstep, optdef
                                            )
                                        }
                                    };
                                }
                                None => {
                                    log::warn!("Could not get option for val {}", val)
                                }
                            }
                        }
                    }
                }
                Err(_) => {
                    log::warn!("got err result in sensorlist")
                }
            }
        }
    }

    Ok(())
}

pub fn setup_logging(verbosity: u64) -> Result<(), fern::InitError> {
    let mut base_config = fern::Dispatch::new().format(|out, message, record| {
        out.finish(format_args!(
            "[{}][{}] {} {}:{} | {}",
            chrono::Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Millis, true),
            record.level(),
            record.target(),
            match record.file() {
                Some(f) => f,
                None => "<nofile>",
            },
            match record.line() {
                Some(l) => l.to_string(),
                None => "0".to_string(),
            },
            message
        ))
    });

    base_config = match verbosity {
        0 => base_config.level(log::LevelFilter::Warn),
        1 => base_config.level(log::LevelFilter::Info),
        2 => base_config.level(log::LevelFilter::Debug),
        _3_or_more => base_config.level(log::LevelFilter::Trace),
    };

    let stdout_config = fern::Dispatch::new()
        .filter(|metadata| metadata.level() != log::LevelFilter::Error)
        .chain(io::stdout());

    let stderr_config = fern::Dispatch::new()
        .level(log::LevelFilter::Error)
        .chain(io::stderr());

    base_config
        .chain(stdout_config)
        .chain(stderr_config)
        .apply()?;

    Ok(())
}

// Since we need to workaround a bug in librealsense we have to re-implement these

#[derive(Debug)]
pub(crate) struct RsErrorChecker {
    checked: bool,
    ptr: *mut realsense_sys::rs2_error,
}

impl RsErrorChecker {
    pub fn new() -> RsErrorChecker {
        RsErrorChecker {
            checked: false,
            ptr: ptr::null_mut(),
        }
    }

    pub fn inner_mut_ptr(&mut self) -> *mut *mut realsense_sys::rs2_error {
        &mut self.ptr as *mut _
    }

    pub fn check(mut self) -> RsResult<()> {
        self.checked = true;
        match NonNull::new(self.ptr) {
            Some(nonnull) => {
                let msg = get_error_message(nonnull);
                let err = if msg.starts_with("Frame didn't arrive within ") {
                    RsError::Timeout(nonnull)
                } else if msg.starts_with("object doesn\'t support option #") {
                    RsError::UnsupportedOption(nonnull)
                } else {
                    RsError::Other(nonnull)
                };
                Err(err)
            }
            None => Ok(()),
        }
    }
}

impl Drop for RsErrorChecker {
    fn drop(&mut self) {
        if !self.checked {
            panic!("internal error: forget to call check()");
        }
    }
}

fn get_error_message<'a>(ptr: NonNull<realsense_sys::rs2_error>) -> &'a str {
    unsafe {
        let ptr = realsense_sys::rs2_get_error_message(ptr.as_ptr());
        CStr::from_ptr(ptr).to_str().unwrap()
    }
}

/// A handle pointing to the option value.
#[derive(Debug, Clone)]
pub struct OptionHandle {
    ptr: NonNull<realsense_sys::rs2_options>,
    option: Rs2Option,
}

impl OptionHandle {
    pub fn get_value(&self) -> RsResult<f32> {
        unsafe {
            let mut checker = RsErrorChecker::new();
            let val = realsense_sys::rs2_get_option(
                self.ptr.as_ptr(),
                self.option as realsense_sys::rs2_option,
                checker.inner_mut_ptr(),
            );
            checker.check()?;
            Ok(val)
        }
    }

    pub fn set_value(&self, value: f32) -> RsResult<()> {
        unsafe {
            let mut checker = RsErrorChecker::new();
            realsense_sys::rs2_set_option(
                self.ptr.as_ptr(),
                self.option as realsense_sys::rs2_option,
                value,
                checker.inner_mut_ptr(),
            );
            checker.check()?;
            Ok(())
        }
    }

    pub fn is_read_only(&self) -> RsResult<bool> {
        unsafe {
            let mut checker = RsErrorChecker::new();
            let val = realsense_sys::rs2_is_option_read_only(
                self.ptr.as_ptr(),
                self.option as realsense_sys::rs2_option,
                checker.inner_mut_ptr(),
            );
            checker.check()?;
            Ok(val != 0)
        }
    }

    pub fn name<'a>(&'a self) -> RsResult<&'a str> {
        unsafe {
            let mut checker = RsErrorChecker::new();
            let ptr = realsense_sys::rs2_get_option_name(
                self.ptr.as_ptr(),
                self.option as realsense_sys::rs2_option,
                checker.inner_mut_ptr(),
            );
            checker.check()?;
            let desc = CStr::from_ptr(ptr).to_str().unwrap();
            Ok(desc)
        }
    }

    pub fn option_description<'a>(&'a self) -> RsResult<&'a str> {
        unsafe {
            let mut checker = RsErrorChecker::new();
            let ptr = realsense_sys::rs2_get_option_description(
                self.ptr.as_ptr(),
                self.option as realsense_sys::rs2_option,
                checker.inner_mut_ptr(),
            );
            checker.check()?;
            let desc = CStr::from_ptr(ptr).to_str().unwrap();
            Ok(desc)
        }
    }

    pub fn value_description<'a>(&'a self, value: f32) -> RsResult<&'a str> {
        unsafe {
            let mut checker = RsErrorChecker::new();
            let ptr = realsense_sys::rs2_get_option_value_description(
                self.ptr.as_ptr(),
                self.option as realsense_sys::rs2_option,
                value,
                checker.inner_mut_ptr(),
            );
            checker.check()?;
            let desc = CStr::from_ptr(ptr).to_str().unwrap();
            Ok(desc)
        }
    }
}
